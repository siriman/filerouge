-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  mysql.info.unicaen.fr:3306
-- Généré le :  Ven 19 Janvier 2018 à 18:48
-- Version du serveur :  5.5.58-0+deb8u1-log
-- Version de PHP :  5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données
--

-- --------------------------------------------------------

--
-- Structure de la table `PERSONS`
--

CREATE TABLE `PERSONS` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `PERSONS`
--

INSERT INTO `PERSONS` (`id`, `name`, `firstname`, `age`, `address`, `job`) VALUES
(1, 'MARTIN', 'DUPONT', 25, 'CITÉ U LEBISEY, 14000 CAEN', 'Developer'),
(2, 'IDIR', 'Alin', 28, 'CITÉ U LEBISEY, 14000 CAEN', 'Artiste'),
(3, 'MANU', 'Daniel', 30, 'PARIS', 'CEO MEET YOU'),
(4, 'ARNAUD', 'Jeanne', 30, 'CITÉ U LEBISEY, 14000 CAEN', 'Commercant'),
(5, 'MOREL', 'Alice', 30, 'CITÉ U LEBISEY, 14000 CAEN', 'Commercant');

-- --------------------------------------------------------

--
-- Structure de la table `RELATIONSHIP`
--

CREATE TABLE `RELATIONSHIP` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `secondPerson_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `RELATIONSHIP`
--

INSERT INTO `RELATIONSHIP` (`id`, `description`, `person_id`, `secondPerson_id`) VALUES
(1, 'MERE', NULL, 1),
(2, 'MERE', 5, 1),
(3, 'MERE', 5, 2),
(4, 'FRERE', 1, 2),
(5, 'PERE', 2, 4);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `PERSONS`
--
ALTER TABLE `PERSONS`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `RELATIONSHIP`
--
ALTER TABLE `RELATIONSHIP`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_20f93918rjqgjss16owhgeary` (`person_id`),
  ADD KEY `FK_4iplrywf7dwillynyj3q7ydla` (`secondPerson_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `RELATIONSHIP`
--
ALTER TABLE `RELATIONSHIP`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `RELATIONSHIP`
--
ALTER TABLE `RELATIONSHIP`
  ADD CONSTRAINT `FK_4iplrywf7dwillynyj3q7ydla` FOREIGN KEY (`secondPerson_id`) REFERENCES `PERSONS` (`id`),
  ADD CONSTRAINT `FK_20f93918rjqgjss16owhgeary` FOREIGN KEY (`person_id`) REFERENCES `PERSONS` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
