package fr.unicaen.info.m2.dnr2i.sqlpersons;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Unit test for SQLPersonDB.
 */
public class SQLPersonDBTest {

    protected SQLPersonDB sqlPersonDB;
    protected PersistentPerson person;

    @Before
    public void setUp() {
        person = new PersistentPerson(null, "MARVIN", "Arnaud",
                22, "Caen", "Student");
        try {
            sqlPersonDB = new SQLPersonDB();
            sqlPersonDB.dropTable();
            sqlPersonDB.createTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
    }

    @Test
    public void insert() throws Exception {
        PersistentPerson result = sqlPersonDB.insert(person);
        System.out.println("INSERT : "+result);

        assertNotEquals(null, result.getId());
        assertEquals(person.getName(), result.getName());
        assertEquals(person.getFirstname(), result.getFirstname());
        assertEquals(person.getAge(), result.getAge());
        assertEquals(person.getAddress(), result.getAddress());
        assertEquals(person.getJob(), result.getJob());
    }

    @Test
    public void select() throws Exception {
        person = sqlPersonDB.insert(person);
        PersistentPerson result = sqlPersonDB.select(person.getId());
        System.out.println("SELECT : " + result);
        assertEquals(person.getId(), result.getId());
    }

    @Test
    public void update() throws Exception {
        person = sqlPersonDB.insert(person);
        person.setAge(25); //before 22
        person.setAddress("Paris"); //before Caen
        person.setJob("Engineer"); //before Student
        PersistentPerson result = sqlPersonDB.update(person);
        System.out.println("UPDATE : " + result);

        assertNotEquals(null, result);
        assertEquals(person.getAge(), result.getAge());
        assertEquals(person.getAddress(), result.getAddress());
        assertEquals(person.getJob(), result.getJob());
    }

    @Test
    public void updateUnExistsPerson() throws Exception {
        PersistentPerson aPerson = new PersistentPerson(3000, "MARVIN", "Arnaud",
                25, "Paris", "Engineer");
        PersistentPerson result = sqlPersonDB.update(aPerson);
        System.out.println("UPDATEUNEXISTSPERSON : " + result);

        assertEquals(null, result);
    }

    @Test
    public void delete() throws Exception {
        person = sqlPersonDB.insert(person);
        boolean res = sqlPersonDB.delete(person.getId());
        System.out.println("DELETE : " + res);
        assertEquals(true, res);
        assertEquals(0, sqlPersonDB.select().size());
    }

    @Test
    public void deleteUnExistsPerson() throws Exception {
        boolean res = sqlPersonDB.delete(10000);
        System.out.println("DELETEUNEXISTSPERSON : " + res);
        assertEquals(false, res);
    }

    @Test
    public void selectAll() throws Exception {
        PersistentPerson aPerson = new PersistentPerson(null, "DUMONT", "Nathalie",
                25, "Paris", "Engineer");
        sqlPersonDB.insert(person);
        sqlPersonDB.insert(aPerson);

        ArrayList<PersistentPerson> res = sqlPersonDB.select();
        System.out.println("SELECT ALL : " + res);
        assertEquals(2, res.size());
    }

    @Test
    public void createTable() throws Exception {
        assertEquals(false, sqlPersonDB.createTable());
    }

    @Test
    public void dropTable() throws Exception {
        assertEquals(false, sqlPersonDB.dropTable());
    }
}