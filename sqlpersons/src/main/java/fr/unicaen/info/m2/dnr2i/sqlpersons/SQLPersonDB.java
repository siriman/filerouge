
package fr.unicaen.info.m2.dnr2i.sqlpersons;

import com.mysql.jdbc.Statement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.models.Person;
import fr.unicaen.info.m2.dnr2i.models.PersonDB;

import java.util.ArrayList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Person database access with JDBC.
 * It implements adapter PersonDB
 */
public class SQLPersonDB implements PersonDB<Person> {
    
    private final Connection connection;
    //DATABASE HOSTNAME
    private final String DB_HOST = "mysql.info.unicaen.fr";
    //DB USERNAME
    private final String USER = "USERNAME"; // REMPLACER USERNAME
    //DB PASSWORD
    private final String PASS = "PASSWORD"; // REMPLACER PASSWORD
    //DB NAME
    private final String DBNAME = "DATABASE_NAME"; // REMPLACER DATABASE_NAME
    //TABLE NAME
    private final String TABLE_PERSONS = "personnes";
    //QUERY FOR TABLE CREATION
    private final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PERSONS + " ( " +
            "    ID INT AUTO_INCREMENT PRIMARY KEY, " +
            "    NAME TEXT NOT NULL, " +
            "    FIRSTNAME TEXT NOT NULL, " +
            "    AGE INT NOT NULL, " +
            "    ADDRESS TEXT NOT NULL, " +
            "    JOB TEXT NOT NULL " +
            ");";
    //TABLE DROP QUERY
    private final String DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE_PERSONS;
    //QUERY FOR SELECT THE 100 FIRST PERSONS
    private final String SELECT_100_QUERY = "SELECT ID, NAME, FIRSTNAME, AGE, ADDRESS, JOB FROM "+ TABLE_PERSONS +" LIMIT 100";
    //QUERY FOR INSERTION
    private final String INSER_QUERY = "INSERT INTO "+ TABLE_PERSONS +" (NAME, FIRSTNAME, AGE, ADDRESS, JOB) VALUES (?, ?, ?, ?, ?)";
    //QEURY FOR SELECT A PERSON BY THIS ID
    private final String SELECT_QUERY = "SELECT ID, NAME, FIRSTNAME, AGE, ADDRESS, JOB FROM " + TABLE_PERSONS + " WHERE ID = ?";
    //QUERY FOR UPDATE A PERSON
    private final String UPDATE_QUERY = "UPDATE " + TABLE_PERSONS + " SET NAME = ?, FIRSTNAME = ?, AGE = ?, ADDRESS = ?, JOB = ? WHERE ID = ?";
    //QUERY FOR DELETE A PERSON
    private final String DELETE_QUERY = "DELETE FROM " + TABLE_PERSONS + " WHERE ID = ?";

    /**
     * Logical constructor.
     *
     * @param connection Connection
     */
    public SQLPersonDB(Connection connection){
        this.connection = connection;
    }

    /**
     * Default constructor.
     *
     * @throws SQLException
     */
    public SQLPersonDB() throws SQLException {
        this.connection = createLink(DB_HOST, DBNAME, USER, PASS);
    }

    /**
     * Return Connection.
     *
     * @param host database hostname
     * @param database database name
     * @param username database username
     * @param password database password
     * @return Connection
     * @throws SQLException if it impossible to establish connection.
     */
    private Connection createLink(String host, String database, String username, String password) throws SQLException {
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName(host);
        ds.setDatabaseName(database);
        //ds.setPortNumber(3306);
        Connection link = ds.getConnection(username, password);
        if (!link.isValid(0)) {
            throw new SQLException("Failed to initialize a valid connection to database.");
        }
        return link;
    }

    @Override
    public PersistentPerson insert(Person person) throws SQLException{
        PreparedStatement preparedStatement = null;
        PersistentPerson persistentPerson = new PersistentPerson(null, 
            person.getName(), person.getFirstname(), person.getAge(), 
                person.getAddress(), person.getJob());
        int response;
        try {
            preparedStatement = connection.prepareStatement(INSER_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, person.getName());
            preparedStatement.setString(2, person.getFirstname());
            preparedStatement.setInt(3, person.getAge());
            preparedStatement.setString(4, person.getAddress());
            preparedStatement.setString(5, person.getJob());
            response = preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            persistentPerson.setId(id);
        } catch (SQLException e){
            throw new SQLException("Erreur lors de l'insertion ", e);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        //System.out.println("INSERT : "+persistentPerson.toString());
        return response == 1 ? persistentPerson : null;
    }

    @Override
    public PersistentPerson select(int personId) throws SQLException{
        PreparedStatement preparedStatement = null;
        PersistentPerson persistentPerson = null;
        try {
             preparedStatement = connection.prepareStatement(SELECT_QUERY);
             preparedStatement.setInt(1, personId);
             ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                persistentPerson = new PersistentPerson();
                persistentPerson.setId(resultSet.getInt("ID"));
                persistentPerson.setName(resultSet.getString("NAME"));
                persistentPerson.setFirstname(resultSet.getString("FIRSTNAME"));
                persistentPerson.setAge(resultSet.getInt("AGE"));
                persistentPerson.setAddress(resultSet.getString("ADDRESS"));
                persistentPerson.setJob(resultSet.getString("JOB"));

                //System.out.println("SELECT : "+persistentPerson.toString());
            }
        } catch(SQLException e){
            throw new SQLException("Echec de l'insertion "+e);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return persistentPerson;
    }

    @Override
    public PersistentPerson update(PersistentPerson person) throws SQLException{
        PreparedStatement preparedStatement = null;
        int response;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, person.getName());
            preparedStatement.setString(2, person.getFirstname());
            preparedStatement.setInt(3, person.getAge());
            preparedStatement.setString(4, person.getAddress());
            preparedStatement.setString(5, person.getJob());
            preparedStatement.setInt(6, person.getId());
            response = preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new SQLException("Erreur lors de la mise a jour ", e);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

        return response == 1 ? person : null;
    }

    @Override
    public boolean delete(int personId) throws SQLException{
        PreparedStatement preparedStatement = null;
        int response;
        try {
            preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setInt(1, personId);
            response = preparedStatement.executeUpdate();
            //System.out.println("DELETE : "+response);
        } catch (SQLException e){
            throw new SQLException("Erreur lors de la suppression ", e);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return response == 1;
    }

    @Override
    public ArrayList<PersistentPerson> select() throws SQLException{
        PreparedStatement preparedStatement = null;
        ArrayList<PersistentPerson> results = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(SELECT_100_QUERY);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PersistentPerson personWithId = new PersistentPerson();
                personWithId.setId(resultSet.getInt("ID"));
                personWithId.setName(resultSet.getString("NAME"));
                personWithId.setFirstname(resultSet.getString("FIRSTNAME"));
                personWithId.setAge(resultSet.getInt("AGE"));
                personWithId.setAddress(resultSet.getString("ADDRESS"));
                personWithId.setJob(resultSet.getString("JOB"));
                results.add(personWithId);
            }
        } catch(SQLException e){
            throw new SQLException("Echec de la recuperation des donnees "+e);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        //System.out.println("ALL : "+results);
        return results;
    }

    /**
     * Create table.
     *
     * @return false if query's execution is successful.
     * @throws SQLException if sql error found.
     */
    public boolean createTable() throws SQLException {
        PreparedStatement preparedStatement = null;
        boolean response = true;
        try {
            preparedStatement = connection.prepareStatement(CREATE_TABLE);
            response = preparedStatement.execute();
            //System.out.println("CREATE RESPONSE "+response);
        } catch(SQLException e){
            throw new SQLException("Impossible de creer la table", e);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return response;
    }

    /**
     * Drop table.
     *
     * @return false if query's execution is successful.
     * @throws SQLException if sql error found.
     */
    public boolean dropTable() throws  SQLException {
        PreparedStatement preparedStatement = null;
        boolean response = true;
        try {
            preparedStatement = connection.prepareStatement(DROP_TABLE);
            response = preparedStatement.execute();
            //System.out.println("DROP RESPONSE "+response);
        } catch(SQLException e){
            throw new SQLException("Impossible de supprimer la table", e);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return response;
    }
}
