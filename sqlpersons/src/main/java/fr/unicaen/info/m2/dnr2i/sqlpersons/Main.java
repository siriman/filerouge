package fr.unicaen.info.m2.dnr2i.sqlpersons;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;

import java.sql.Connection;
import java.sql.SQLException;


public class Main {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_HOST = "mysql.info.unicaen.fr";
    private static final String USER = "USERNAME"; // REMPLACER USERNAME
    private static final String PASS = "PASSWORD"; // REMPLACER PASSWORD
    private static final String DBNAME = "DATABASE_NAME"; // REMPLACER DATABASE_NAME

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Connection connection = null;
        try {
            connection = createLink(DB_HOST, DBNAME, USER, PASS);
            checkCreateAndDropTable(connection);
            /**
             * ##### DECCOMMENTER LES LIGNES CI-DESSOUS POUR TESTES. #####
             */
            //checkInsert(connection);
            //checkSelect(connection);
            //checkUpdate(connection);
            //checkDelete(connection);
            //checkSelectAll(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static Connection createLink(String host, String database, String username, String password) throws SQLException {
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName(host);
        ds.setDatabaseName(database);
        //ds.setPortNumber(3306);
        Connection link = ds.getConnection(username, password);
        if (!link.isValid(0)) {
            throw new SQLException("Failed to initialize a valid connection to database.");
        }
        return link;
    }

    private static void checkCreateAndDropTable(Connection connection) {
        SQLPersonDB sqlPersonDB = new SQLPersonDB(connection);
        try {
            sqlPersonDB.dropTable();
            sqlPersonDB.createTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void checkInsert(Connection connection) {
        PersistentPerson person = new PersistentPerson(null, "MARVIN", "Arnaud",
                30, "Caen", "Manager");
        SQLPersonDB sqlPersonDB = new SQLPersonDB(connection);
        try {
            sqlPersonDB.insert(person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void checkSelect(Connection connection) {
        SQLPersonDB sqlPersonDB = new SQLPersonDB(connection);
        try {
            sqlPersonDB.select(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void checkSelectAll(Connection connection) {
        SQLPersonDB sqlPersonDB = new SQLPersonDB(connection);
        try {
            sqlPersonDB.select();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void checkUpdate(Connection connection) {
        PersistentPerson person = new PersistentPerson(1, "MARIE", "Jeanne",
                21, "Rue de Lebisey", "Student");
        SQLPersonDB sqlPersonDB = new SQLPersonDB(connection);
        try {
            sqlPersonDB.update(person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void checkDelete(Connection connection) {
        SQLPersonDB sqlPersonDB = new SQLPersonDB(connection);
        try {
            sqlPersonDB.delete(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
