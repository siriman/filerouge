# PROJET ANNUAIRE PERSONNE en JEE

## Auteurs
- Siriman 	TRAORE 			21308390
- Lotfi 		IDIR					21613320

## Présentation
Ce projet consiste à mettre en place une application annuaire via différent procédé. 
Il contient des modules suivants :

 1. **library** : Il s'agit du module qui représente la logique métier en quelque sorte. Il contient les entités représentant une personne ainsi que l'adaptateur permettant de faciliter la mise en place d'un système de stockage sur ce dernier.
 2. **webservice** : qui représente une application api-rest développé en JEE natif. 
 3. **rest-api** : qui est une api-rest développé avec Spring.
 4. **sqlpersons** : est une librairie permettant de gérer la persistance des personnes utilisant JDBC.
 5. **personsHibernate** : comme **sqlpersons**,  il s'agit d'une librairie permettant de gérer la persistance des personnes utilisant le framework Hibernate.
 6. **personsJsf** : est une application mvc JEE avec JSF.

## Configuration
Éditer les fichiers ci-dessous afin de mettre les accès à la base de données.  Il s'agit de :
- `personsHibernate/src/main/resources/hibernate.cfg.xml`
- `sqlpersons/src/main/java/fr/unicaen/info/m2/dnr2i/sqlpersons/Main.java`
- `sqlpersons/src/main/java/fr/unicaen/info/m2/dnr2i/sqlpersons/SQLPersonDB.java`

Il peut être commode de remplir la base de données (cela n'est pas obligatoire), au souhait, il vous suffit d'utiliser le fichier `data.sql` : 
- en ligne de commande `mysql` par `source data.sql` ou de le charger via `phpmydamin`.

## Utilisations
- `mvn package` pour générer les .war et .jar avec exécution des tests unitaires.
- `mvn package  -Dmaven.test.skip=true` pour générer les .war et jar sans exécuter les tests unitaires. 
### Déploiements
##### Création du domaine
`asadmin create-domain --domaindir $PATH_TO_DOMAIN_DIR domain-siriman`
##### Lancement du domaine
`asadmin start-domain --domaindir $PATH_TO_DOMAIN_DIR domain-siriman`
##### Déploiement
Se mettre dans la racine du projet et exécuter les commandes ci-dessous :

- `cp webservice/target/webservice.war ~/domain-siriman/autodeploy/`
- `cp rest-api/target/rest-api.war ~/domain-siriman/autodeploy/`
- `cp personsJsf/target/webservice.war ~/domain-siriman/autodeploy/`

### webservice 
WebService avec JEE natif.
Les différentes routes sont : 

 - `/get?id={personId} en GET` pour récupérer une personne
 - `/person en POST` pour ajouter une personne
 - `/update en POST` pour mettre à jour une personne
 - `/persons en GET` pour la liste des personnes
 - `/delete?id={personId} en GET` pour supprimer une personne

Pour tester le webservice vous pouvez utiliser les extensions **Firefox** ou **Chrome** comme **RESTer** (firefox), **POSTMAN** (chrome) qui permettent de faire des réquêtes HTTP.

Il faudrait ajouter dans les headers pour l'ajout et l'édition la clé `Content-Type` avec la valeur `application/x-www-form-urlencoded` et passer les données dans la BODY via l'interface de l'utilitaire.

### rest-api 
L'api REST avec Spring, la librairie **personsHibernate** et la librairie **library**

Pour le tester vous pouvez utiliser les extensions **Firefox** ou **Chrome** comme **RESTer** (firefox), **POSTMAN** (chrome) qui permettent de faire des requêtes HTTP.

Il faudrait ajouter dans les headers pour l'ajout et l'édition la clé `Content-Type` avec la valeur `application/json` et passer les données dans la BODY via l'interface de l'utilitaire.

Les routes sont : 

- `/api/{personId} en GET` pour récupérer une personne
- `/api/person en POST` pour ajouter une personne.
- `/api/person en PUT` pour mettre à jour une personne
- `/api/delete/{personId} en DELETE` pour supprimer une personne
- `/api/persons en GET` pour récupérer la liste des personnes

#### Exemples 
- Exemple de données passées en body pour l'ajout 
`    {
        "name": "IDIR",
        "firstname": "Alin",
        "age": 28,
        "address": "Campus 2, 14000 CAEN",
        "job": "Artiste"
    }`
    
 - Exemple de données passées en body pour l'édition `    {
        "name": "IDIR",
        "firstname": "Alin",
        "age": 28,
        "address": "CITÉ U LEBISEY, 14000 CAEN",
        "job": "Ingénieur logiciel",
        "relations": [],
        "id": 2
    }`

- Exemple de données passées en body pour l'édition avec ajout de relations ` {
    "name": "IDIR",
    "firstname": "Alin",
    "age": 28,
    "address": "CITÉ U LEBISEY, 14000 CAEN",
    "job": "Artiste",
    "relations": [
        {
	        "description":"PERE",
	        "person": {
                "name": "IDIR",
                "firstname": "Alin",
                "age": 28,
                "address": "CITÉ U LEBISEY, 14000 CAEN",
                "job": "Artiste",
                "relations": [],
                "id": 2
            }, 
            "secondPerson": {
                "name": "ARNAUD",
                "firstname": "Jeanne",
                "age": 30,
                "address": "CITÉ U LEBISEY, 14000 CAEN",
                "job": "Commercant",
                "relations": [],
                "id": 4
            } 
		}
    ],
    "id": 2
}
`

### Autres détails 
Les applications **webservice**, **rest-api** et **personsJsf** après déploiement sur le serveur  sont accessibles sur les urls : 

- `http://localhost:PORT/webservice`
- `http://localhost:PORT/rest-api/api`
- `http://localhost:PORT/personsJsf`

Dans notre modélisation de la relation, nous sommes partis sur le choix d'une relation unidirectionnelle même si dans la pratique elle peut-être bidirectionnelle. Il s'agit d'un choix de notre part, tout à fait discutable.


