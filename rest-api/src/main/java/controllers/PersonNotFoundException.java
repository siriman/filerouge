package controllers;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * HTTP 404 error when person not found.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class PersonNotFoundException extends RuntimeException {

    /**
     * Logical constructor.
     *
     * @param personId person's identifier.
     */
    public PersonNotFoundException(int personId) {
        super("Could not find person '" + personId + "'.");
    }
}
