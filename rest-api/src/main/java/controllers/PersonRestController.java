package controllers;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.models.Person;
import fr.unicaen.info.m2.dnr2i.personsHibernate.PersonHibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Controller person in mode Restful
 */
@RestController
@RequestMapping("/api")
public class PersonRestController {

    /**
     * Instance of PersonHibernate for database exchange
     */
    @Autowired
    private PersonHibernate personHibernate;

    /**
     * Find a person
     *
     * @param personId identifier of a person
     * @return {@link fr.unicaen.info.m2.dnr2i.models.PersistentPerson}
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{personId}")
    public PersistentPerson getPerson(@PathVariable int personId ){
        PersistentPerson person = null;
        try {
            person = personHibernate.select(personId);
        } catch (SQLException e) {
            //Show Error 500
            throw new InternalErrorException(e);
        }
        //Check if person is found
        if (person == null )
            throw new PersonNotFoundException(personId);
        return person;
    }

    /**
     * Add a person into the database.
     *
     * @param input {@link fr.unicaen.info.m2.dnr2i.models.Person}
     * @return {@link fr.unicaen.info.m2.dnr2i.models.PersistentPerson}
     */
    @RequestMapping(method = RequestMethod.POST, value = "/person")
    public PersistentPerson addPerson( @RequestBody Person input ){
        PersistentPerson person = null;
        try {
            person = personHibernate.insert(input);
        } catch (SQLException e) {
            //Show Error 500
            throw new InternalErrorException(e);
        }
        return person;
    }

    /**
     * Update a person.
     *
     * @param input {@link fr.unicaen.info.m2.dnr2i.models.PersistentPerson}
     * @return {@link fr.unicaen.info.m2.dnr2i.models.PersistentPerson}
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/person")
    public PersistentPerson updatePerson( @RequestBody PersistentPerson input ){
        PersistentPerson person = null;
        try {
            person = personHibernate.update(input);
        } catch (SQLException e) {
            //Show Error 500
            throw new InternalErrorException(e);
        }
        if (person == null )
            throw new PersonNotFoundException(input.getId());
        return input;
    }

    /**
     * Delete a person
     *
     * @param personId person's identifier
     * @return true if deletion is OK else false.
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{personId}")
    public boolean deletePerson( @PathVariable int personId ){
        boolean rest = false;
        try {
            rest = personHibernate.delete(personId);
        } catch (SQLException e) {
            //Show Error 500
            throw new InternalErrorException(e);
        }
        return rest;
    }

    /**
     * Return a list of person
     * The best practice was to pass the parameter that indicate the size of the results, ie 20 for the 20 first results.
     *
     * @return ArrayList<PersistentPerson>
     */
    @RequestMapping(method = RequestMethod.GET, value = "/persons")
    public ArrayList<PersistentPerson> getPersons(){
        ArrayList<PersistentPerson> persons = new ArrayList<>();
        try {
            persons = personHibernate.select();
        } catch (SQLException e) {
            //Show Error 500
            throw new InternalErrorException(e);
        }
        return persons;
    }
}