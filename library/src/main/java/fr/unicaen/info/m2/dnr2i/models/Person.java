package fr.unicaen.info.m2.dnr2i.models;

import java.util.HashSet;
import java.util.Set;

/**
 * Class represents Person.
 *
 */
public class Person {

    /**
     * Default constructor.
     */
    public Person(){
        this("", "", 0, "", "");
    }

    /**
     * Logical constructor.
     * 
     * @param name the value for {@link Person#name} 
     * @param firstname the value for {@link Person#firstname}
     * @param age the value for {@link Person#age}
     * @param address the value for {@link Person#address}
     * @param job the value for {@link Person#job}
     */
    public Person(String name, String firstname, int age, String address, 
            String job){
            this.name = name;
            this.firstname = firstname;
            this.age = age;
            this.address = address;
            this.job = job;
            this.relations = new HashSet<>();
    }

    /**
     * Accessor 
     * 
     * @return {@link Person#name}
     */
    public String getName() {
        return name;
    }

    /**
     * Accessor 
     * 
     * @param name {@link Person#name}
     */
    public void setName(String name) {
            this.name = name;
    }

    /**
     * Accessor 
     * 
     * @return {@link Person#firstname}
     */
    public String getFirstname() {
            return firstname;
    }

    /**
     * Accessor 
     * 
     * @param firstname {@link Person#firstname}
     */
    public void setFirstname(String firstname) {
            this.firstname = firstname;
    }

    /**
     * Accessor 
     * 
     * @return {@link Person#age}
     */
    public int getAge() {
            return age;
    }

    /**
     * Accessor 
     * 
     * @param age {@link Person#age}
     */
    public void setAge(int age) {
            this.age = age;
    }

    /**
     * Accessor 
     * 
     * @return {@link Person#address}
     */
    public String getAddress() {
            return address;
    }

    /**
     * Accessor 
     * 
     * @param address {@link Person#address}
     */
    public void setAddress(String address) {
            this.address = address;
    }

    /**
     * Accessor 
     * 
     * @return {@link Person#job}
     */
    public String getJob() {
            return job;
    }

    /**
     * Accessor 
     * 
     * @param job {@link Person#job}
     */
    public void setJob(String job) {
            this.job = job;
    }


    /**
     * Accessor
     *
     * @return {@link Person#job}
     */
    public Set<Relationship> getRelations() {
        return relations;
    }

    /**
     * Accessor
     *
     * @param relations {@link Person#job}
     */
    public void setRelations(Set<Relationship> relations) {
        this.relations = relations;
    }

    @Override
    public String toString() {
        return "{\"name\": \"" + name + "\"" + ", \"firstname\": \"" + firstname + "\"" + ", \"age\": " + age + ", \"address\": \"" + address + "\"" + ", \"job\": \"" + job + "\"}";
    }	
    
    /**
     * This person name.
     */
    protected String name;
    
    /**
     * This person firstname.
     */
    protected String firstname;
    
    /**
     * This person age.
     */
    protected int age;
    
    /**
     * The adress of this person.
     */
    protected String address;
    
    /**
     * The job for this person.
     */
    protected String job;
    
    /**
     * The relations of this person.
     */
    protected Set<Relationship> relations = new HashSet<Relationship>();
}
