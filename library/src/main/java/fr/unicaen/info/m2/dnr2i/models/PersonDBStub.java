package fr.unicaen.info.m2.dnr2i.models;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class simulate the database storage for person.
 * It implements Adapter PersonDB.
 */
public class PersonDBStub implements PersonDB <Person> {

    /**
     * List of persons.
     * The identifier for the person is = of person position in persons array.
     */
    private ArrayList<PersistentPerson> persons;

    /**
     * Index for person item.
     */
    private int index;

    /**
     * Default constructor.
     */
    public PersonDBStub() throws SQLException {
        persons = new ArrayList<>();
        insert(new Person("DUPONT", "Alain", 30, "Blvd Marechal Juin", "Admin Database"));
        insert(new Person("DUCRET", "Arnaud", 25, "Blvd G. De Gaulle", "Manager"));
    }

    @Override
    public PersistentPerson insert(Person person) throws SQLException {
        index++;
        PersistentPerson persistentPerson = new PersistentPerson(index,
                person.getName(), person.getFirstname(), person.getAge(),
                person.getAddress(), person.getJob());
        persistentPerson.setRelations(person.getRelations());
        persons.add(persistentPerson);
        return persistentPerson;
    }

    @Override
    public PersistentPerson update(PersistentPerson person) throws SQLException {
        PersistentPerson persistentPerson = select(person.getId());
        //Check if person exists
        if(persistentPerson != null){
            persistentPerson.setName(person.getName());
            persistentPerson.setFirstname(person.getFirstname());
            persistentPerson.setAge(person.getAge());
            persistentPerson.setAddress(person.getAddress());
            persistentPerson.setJob(person.getJob());
            return person;
        }
        return null;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        for (PersistentPerson person: persons){
            //Check if person exists
            if(person.getId() == id){
                persons.remove(person);
                return true;
            }
        }
        return false;
    }

    @Override
    public PersistentPerson select(int personId) throws SQLException {
        for (PersistentPerson person : persons){
            if(person.getId() == personId)
                return person;
        }
        return null;
    }

    @Override
    public ArrayList<PersistentPerson> select() throws SQLException {
        return persons;
    }
}
