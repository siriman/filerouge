
package fr.unicaen.info.m2.dnr2i.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Class represents the relation between the persons.
 *
 */
public class Relationship {

    /**
     * Logical constructor.
     * 
     * @param id the value of {@link Relationship#id}
     * @param description the value of {@link Relationship#description}
     * @param person the value of {@link Relationship#person}
     * @param secondPerson the value of {@link Relationship#secondPerson}
     */
    public Relationship(Integer id, String description, PersistentPerson person, 
            PersistentPerson secondPerson) {
        this.id = id;
        this.description = description;
        this.person = person;
        this.secondPerson = secondPerson;
    }

    /**
     * Default constructor.
     */
    public Relationship() {
        this(null, "", null, null);
    }

    /**
     * Accessor
     *
     * @return {@link Relationship#id}
     */
    public Integer getId() {
        return id;
    }

    /**
     * Accessor
     * 
     * @param id {@link Relationship#id}
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * Accessor
     * 
     * @return {@link Relationship#description}
     */
    public String getDescription() {
        return description;
    }

    /**
     * Accessor
     * 
     * @param description {@link Relationship#description} 
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public PersistentPerson getPerson() {
        return person;
    }

    public void setPerson(PersistentPerson person) {
        this.person = person;
    }


    /**
     * Accessor
     *
     * @return {@link Relationship#secondPerson}
     */
    public Person getSecondPerson() {
        return secondPerson;
    }

    /**
     * Accessor
     *
     * @param secondPerson  {@link Relationship#secondPerson}
     */
    public void setSecondPerson(PersistentPerson secondPerson) {
        this.secondPerson = secondPerson;
    }
    
    /**
     * This relation name.
     */
    protected String description;
    
    /**
     * A first person.
     */
    @JsonIgnore
    protected PersistentPerson person;
    
    /**
     * A second person.
     */
    protected PersistentPerson secondPerson;
    
    /**
     * This realation identifiant.
     */
    protected Integer id;
}
