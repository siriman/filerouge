package fr.unicaen.info.m2.dnr2i.models;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Adapter for Person storage system.
 *
 * @param <Person> type of entity.
 */
public interface PersonDB <Person> {

    /**
     * Add a person.
     *
     * @param person person to add.
     * @return PersistentPerson
     * @throws SQLException can launch SQLException
     */
    PersistentPerson insert(Person person) throws SQLException;
    
    /**
     * Update a person
     *
     * @param person A person
     * @return PersistentPerson or null
     * @throws SQLException can launch SQLException
     */
    PersistentPerson update(PersistentPerson person) throws SQLException;
    
    /**
     * Delete person on a Database.
     *
     * @param personId person identifier
     * @return true if deletion is ok, else false.
     * @throws SQLException can launch SQLException
     */
    boolean delete(int personId) throws SQLException;
    
    /**
     * Return a person if person exists, else null.
     *
     * @param personId identifier of a person
     * @return PersistentPerson or null
     * @throws SQLException can launch SQLException
     */
    PersistentPerson select(int personId) throws SQLException;
    
    /**
     * Return list of persons.
     *
     * @return ArrayList<PersistentPerson>
     * @throws SQLException can launch SQLException
     */
    ArrayList<PersistentPerson> select() throws SQLException;
}
