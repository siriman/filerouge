package fr.unicaen.info.m2.dnr2i.models;

/**
 * Represents the person with identifier
 */
public class PersistentPerson extends Person {

    /**
     * Identifier
     */
    protected Integer id;

    /**
     * Logical constructor.
     *
     * @param id value of {@link PersistentPerson#id}
     * @param name value of {@link Person#name}
     * @param firstname value of {@link Person#firstname}
     * @param age value of {@link Person#age}
     * @param address value of {@link Person#address}
     * @param job value of {@link Person#job}
     */
    public PersistentPerson(Integer id, String name, String firstname, int age, 
            String address, String job){
        super(name, firstname, age, address, job);
        this.id = id;
    }

    /**
     * Default constructor.
     */
    public PersistentPerson(){
        this(null, "", "", 0, "", "");
    }

    /**
     * Accessor
     *
     * @return {@link PersistentPerson#id}
     */
    public Integer getId(){
        return id;
    }

    /**
     * Accessor
     * 
     * @param id value of {@link PersistentPerson#id}
     */
    public void setId(Integer id){
        this.id = id;
    }
    
    @Override
    public String toString() {
        return "{\"id\": " + id + ", \"name\": \"" + name + "\"" + ", \"firstname\": \"" + firstname + "\"" + ", \"age\": " + age + ", \"address\": \"" + address + "\"" + ", \"job\": \"" + job + "\"}";
    }
}
