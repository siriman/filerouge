package servlets;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.models.Person;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Represents the servlet for person updating.
 */
public class UpdatePersonServlet extends AbstractPersonServlet {
    @Override
    public PersistentPerson addPerson(Person person) {
        return null;
    }

    @Override
    public PersistentPerson getPerson(int id) {
        return null;
    }

    @Override
    public PersistentPerson updatePerson(PersistentPerson person) {
        PersistentPerson persistentPerson = null;
        try {
            persistentPerson = DBProuder.getInstance().getPersonDBStub().update(person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return persistentPerson;
    }

    @Override
    public boolean deletePerson(int id) {
        return false;
    }

    @Override
    public ArrayList<PersistentPerson> getAllPersons() {
        return null;
    }
}
