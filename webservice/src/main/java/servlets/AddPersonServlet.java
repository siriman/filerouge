package servlets;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.models.Person;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Represents the servlet for adding person
 */
public class AddPersonServlet extends AbstractPersonServlet {

    @Override
    public PersistentPerson addPerson(Person person) {
        PersistentPerson persistentPerson = null;
        try {
            persistentPerson = DBProuder.getInstance().getPersonDBStub().insert(person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return persistentPerson;
    }

    @Override
    public PersistentPerson getPerson(int id) {
        return null;
    }

    @Override
    public PersistentPerson updatePerson(PersistentPerson persistentPerson) {
        return null;
    }

    @Override
    public boolean deletePerson(int id) {
        return false;
    }

    @Override
    public ArrayList<PersistentPerson> getAllPersons() {
        return null;
    }
}
