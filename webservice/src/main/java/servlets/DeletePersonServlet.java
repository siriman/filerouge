package servlets;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.models.Person;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Represents the servlet for person deletion.
 */
public class DeletePersonServlet extends AbstractPersonServlet {

    @Override
    public PersistentPerson addPerson(Person person) {
        return null;
    }

    @Override
    public PersistentPerson getPerson(int id) {
        return null;
    }

    @Override
    public PersistentPerson updatePerson(PersistentPerson persistentPerson) {
        return null;
    }

    @Override
    public boolean deletePerson(int id) {
        boolean response = false;
        try {
            response = DBProuder.getInstance().getPersonDBStub().delete(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public ArrayList<PersistentPerson> getAllPersons() {
        return null;
    }
}
