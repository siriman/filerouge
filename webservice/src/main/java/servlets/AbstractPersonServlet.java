package servlets;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.models.Person;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Abstract class that represents the servlets for ours app
 * It's implements the design pattern « Template Method »
 */
public abstract class AbstractPersonServlet extends HttpServlet{

	/**
	 * Route for add person.
	 */
	public static final String ADD_PATH 	= "/person";

	/**
	 * Route for get a person.
	 */
	public static final String GET_PATH 	= "/get";

	/**
	 * Route for update a person.
	 */
	public static final String UPDATE_PATH 	= "/update";

	/**
	 * Route for delete a person.
	 */
	public static final String DELETE_PATH 	= "/delete";

	/**
	 * Route for get list of person.
	 */
	public static final String ALL_PATH 	= "/persons";

	/**
	 * Get Request
	 *
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ){
		String servletPath = request.getServletPath();

		//Action for route => /get?id={id}
		if(servletPath.equals(GET_PATH)){
			try {
				int id = Integer.parseInt(request.getParameter("id"));
				//Call of the template method getPerson
				PersistentPerson persistentPerson = getPerson(id);
				String data = persistentPerson != null ? persistentPerson.toString() : "{\"message\": \"Not found\"}";
				writeResponse(response, data);
			} catch (NumberFormatException e){
				e.printStackTrace();
			}
			return;
		}

		//Action for route => /all
		if(servletPath.equals(ALL_PATH)){
			try {
				//Call of the template method getAllPersons
				ArrayList<PersistentPerson> persons = getAllPersons();
				writeResponse(response, persons.toString());
			} catch (NumberFormatException e){
				e.printStackTrace();
			}
			return;
		}

		//Action for route => /delete
		if(servletPath.equals(DELETE_PATH)){
			try {
				int id = Integer.parseInt(request.getParameter("id"));
				//Call of the template method deletePerson
				boolean result = deletePerson(id);
				String data = result ? "{\"message\": \"Deletion successful\"}" : "{\"message\": \"Not found\"}";
				writeResponse(response, data);
			} catch (NumberFormatException e){
				e.printStackTrace();
			}
		}
	}

	/**
	 * Post Request
	 *
	 * @param request HttpServletRequest
	 * @param resp HttpServletResponse
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse resp){
		String name = request.getParameter("name");
		String firstname = request.getParameter("firstname");
		String address = request.getParameter("address");
		String job = request.getParameter("job");
		int age = 18;
		try {
			age = Integer.parseInt(request.getParameter("age"));
		} catch (NumberFormatException e){
			e.printStackTrace();
		}

		String servletPath = request.getServletPath();
		//Action for route => /person in POST
		if(servletPath.equals(ADD_PATH)){
			Person input = new Person(name, firstname, age, address, job);
			//Call of the template method addPerson
			PersistentPerson persistentPerson = addPerson(input);
			writeResponse(resp, persistentPerson.toString());
			return;
		}

		//Action for route => /update in POST
		if(servletPath.equals(UPDATE_PATH)){
			try {
				int id = Integer.parseInt(request.getParameter("id"));
				PersistentPerson input = new PersistentPerson(id, name, firstname, age, address, job);
				//Call of the template method updatePerson
				PersistentPerson persistentPerson = updatePerson(input);
				String data = persistentPerson != null ? persistentPerson.toString() : "{\"message\": \"Not found\"}";
				writeResponse(resp, data);
			} catch (NumberFormatException e){
				e.printStackTrace();
			}
			return;
		}
	}

	/**
	 * Write output in JSON
	 *
	 * @param response HttpServletResponse
	 * @param data result of the request
	 */
	public void writeResponse( HttpServletResponse response, String data){
		response.setContentType("application/json");
		response.setCharacterEncoding( "UTF-8" );
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.println(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add person with the routing « POST /pserson »
	 *
	 * @param person a person
	 * @return PersistentPerson
	 */
	public abstract PersistentPerson addPerson(Person person);

	/**
	 * Return a person or null on « GET /get?id={personid}
	 *
	 * @param id identifier
	 * @return PersistentPerson
	 */
	public abstract PersistentPerson getPerson(int id);

	/**
	 * Update a person on « POST /update »
	 *
	 * @param persistentPerson a person
	 * @return PersistentPerson
	 */
	public abstract PersistentPerson updatePerson(PersistentPerson persistentPerson);

	/**
	 * Delete a person on route « GET /delete »
	 *
	 * @param id identifier
	 * @return true if deletion is successfull or false
	 */
	public abstract boolean deletePerson(int id);

	/**
	 * Give all person « /all »
	 *
	 * @return ArrayList<PersistentPerson>
	 */
	public abstract ArrayList<PersistentPerson> getAllPersons();
}
