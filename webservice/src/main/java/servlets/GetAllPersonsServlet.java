package servlets;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.models.Person;

import java.sql.SQLException;
import java.util.ArrayList;

public class GetAllPersonsServlet extends AbstractPersonServlet {
    @Override
    public PersistentPerson addPerson(Person person) {
        return null;
    }

    @Override
    public PersistentPerson getPerson(int id) {
        return null;
    }

    @Override
    public PersistentPerson updatePerson(PersistentPerson persistentPerson) {
        return null;
    }

    @Override
    public boolean deletePerson(int id) {
        return false;
    }

    @Override
    public ArrayList<PersistentPerson> getAllPersons() {
        ArrayList<PersistentPerson> persons = new ArrayList<>();
        try {
            persons = DBProuder.getInstance().getPersonDBStub().select();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return persons;
    }
}
