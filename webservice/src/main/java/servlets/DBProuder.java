package servlets;

import fr.unicaen.info.m2.dnr2i.models.*;

import java.sql.SQLException;

/**
 * Singleton for PersonDBStub access.
 *
 * This class implements the design pattern Singleton.
 */
public class DBProuder {

	/**
	 * Instance of PersonDBStub.
	 */
    private volatile PersonDBStub personDBStub;

	/**
	 * Class instance
	 */
	private static volatile DBProuder instance = null;

	/**
	 * Private default constructor
	 */
	private DBProuder(){
		try {
			this.personDBStub = new PersonDBStub();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Return the uniq instance of class
	 *
	 * @return {@link DBProuder}
	 */
	public synchronized static final DBProuder getInstance(){
		if(instance == null)
			instance = new DBProuder();
		return instance;
	}

	/**
	 * Getter
	 *
	 * @return {@link DBProuder#personDBStub}
	 */
	public PersonDBStub getPersonDBStub(){
		return personDBStub;
	}
}
