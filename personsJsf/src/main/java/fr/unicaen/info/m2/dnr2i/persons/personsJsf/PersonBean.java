
package fr.unicaen.info.m2.dnr2i.persons.personsJsf;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.personsHibernate.PersonHibernate;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents the bean of Person item.
 * 
 * It's in requestscoped.
 */
@ManagedBean (name="personBean", eager = true)
@RequestScoped
public class PersonBean {
    
    @ManagedProperty (value = "#{param.id}") 
    private int identifiant;
    
    /**
     * A person.
     */
    private PersistentPerson person;
    
    /**
     * Database manage with hibernate.
     */
    private final PersonHibernate personHibernate;

    /**
     * Default constructor.
     */
    public PersonBean(){
        this.personHibernate = new PersonHibernate();
    }

    /**
     * Accessor.
     * 
     * @return {@link PersonBean#identifiant}
     */
    public int getIdentifiant() {
        return identifiant;
    }

    /**
     * Accessor.
     * 
     * @param identifiant value for {@link PersonBean#identifiant}
     */
    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
        try {
            this.person = personHibernate.select(identifiant);
        } catch (SQLException ex) {
            Logger.getLogger(PersonBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Accessor.
     * 
     * @return {@link PersonBean#person}
     */
    public PersistentPerson getPerson() {
        return person;
    }  
    
    /**
     * Load person in database.
     */
    public void load(){
        try {
            this.person = personHibernate.select(identifiant);
        } catch (SQLException ex) {
            Logger.getLogger(PersonBean.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
