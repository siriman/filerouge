package fr.unicaen.info.m2.dnr2i.persons.personsJsf;


import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.personsHibernate.PersonHibernate;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Represents a bean for home page.
 */
@ManagedBean (name="personsBean", eager = true)
@RequestScoped
public class PersonsBean {
    
    /**
     * List of person.
     */
    private ArrayList<PersistentPerson> persons;
    
    /**
     * Database manage with hibernate.
     */
    private PersonHibernate personHibernate;
    
    /**
     * Default constructor.
     */
    public PersonsBean(){
        this.personHibernate = new PersonHibernate();
        this.persons = new ArrayList<>();
    }

    /**
     * Accessor.
     * 
     * @return value of {@link PersonsBean#persons}
     */
    public ArrayList<PersistentPerson> getPersons() {
        try {
            this.persons = personHibernate.select();
            System.out.println(persons);
        } catch (SQLException ex) {

        }
        return persons;
    }

    /**
     * Accessor.
     * 
     * @param persons {@link PersonsBean#persons}
     */
    public void setPersons(ArrayList<PersistentPerson> persons) {
        this.persons = persons;
    }

    /**
     * Accessor.
     * 
     * @return {@link PersonsBean#personHibernate}
     */
    public PersonHibernate getPersonHibernate() {
        return personHibernate;
    }

    /**
     * Accessor.
     * 
     * @param personHibernate {@link PersonsBean#personHibernate}
     */
    public void setPersonHibernate(PersonHibernate personHibernate) {
        this.personHibernate = personHibernate;
    }
}
