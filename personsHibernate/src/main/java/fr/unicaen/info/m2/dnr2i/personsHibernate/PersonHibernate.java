package fr.unicaen.info.m2.dnr2i.personsHibernate;

import fr.unicaen.info.m2.dnr2i.models.PersistentPerson;
import fr.unicaen.info.m2.dnr2i.models.Person;
import fr.unicaen.info.m2.dnr2i.models.PersonDB;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.sql.SQLException;
import java.util.ArrayList;


/**
 * 
 */
public class PersonHibernate implements PersonDB <Person>{

    private SessionFactory sessionFactory;

    public PersonHibernate() {
    }

    @Override
    public PersistentPerson insert(Person person) throws SQLException {
        this.initialize();
        Session session = sessionFactory.openSession();
        PersistentPerson persistentPerson = new PersistentPerson(null, 
            person.getName().toUpperCase(), person.getFirstname(), person.getAge(), 
                person.getAddress(), person.getJob());
        persistentPerson.setRelations(person.getRelations());
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(persistentPerson);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
            this.close();
        }
        return persistentPerson;
    }

    @Override
    public PersistentPerson update(PersistentPerson person) throws SQLException {
        this.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(person);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
            this.close();
        }
        
        return person;
    }

    @Override
    public boolean delete(int personId) throws SQLException {        
        this.initialize();

        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Object persistentInstance = session.load(PersistentPerson.class, personId);
            //persistentPerson = (PersistentPerson) session.load(PersistentPerson.class, personId);
            if (persistentInstance != null) {
                session.delete(persistentInstance);
                transaction.commit();
                return true;
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
            this.close();
        }
        
        return false;
    }

    @Override
    public PersistentPerson select(int personId) throws SQLException {
        this.initialize();
        PersistentPerson persistentPerson = null;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            persistentPerson = (PersistentPerson) session.load(PersistentPerson.class, personId);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
            this.close();
        }
        
        return persistentPerson;
    }

    @Override
    public ArrayList<PersistentPerson> select() throws SQLException {
        this.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        ArrayList<PersistentPerson> allPersons = new ArrayList<>();
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from PersistentPerson");
            allPersons = (ArrayList<PersistentPerson>)query.list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction !=  null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
            this.close();
        }
        return allPersons;
    }


    protected void initialize () throws InvalidMappingException {
        ServiceRegistry serviceRegistry = null;
        try {
            Configuration configuration = new Configuration().configure();
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            serviceRegistry = builder.build();
            this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable e) {
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
            throw e;
        }
    }

    protected void close () throws HibernateException {
        if (this.sessionFactory!=null) {
            this.sessionFactory.close();
        }
    }
}
